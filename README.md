# GitLab Best Practices

Curated list of best practices, conventions, and tips for the work with GitLab.

## Markdown

The [_GitLab Flavored Markdown_](https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references) can be used basically everywhere.

- **Links**: Use the [GitLab-specific references](https://docs.gitlab.com/ee/user/markdown.html#gitlab-specific-references) to link to users (prefix `@`), issues (prefix `#`), merge requests (prefix `!`), milestones (prefix `%`), and projects (suffix `>`). Labels (prefix `~`) and commits (stated verbatim by sha) are additionally rendered and linked as expected.<br>Example: _`~Example` refers to this repository's label ~Example._
- **Cross-Links**: Add the `namespace/repo` prefix for one of the aforementioned links to refer to the corresponding entity from another repository.<br>Example: _`falco.nogatz/gitlab-best-practices~Example` refers to falco.nogatz/gitlab-best-practices~Example from any repository._
- **Syntax-Highlighting**: Indicate the programming language when stating code blocks with ` ```language`.<br>Example: _` ```html` indicates an HTML snippet which gets accordingly syntax-highlighted._
- **Style**: Use Prettier with its [built-in Markdown support](https://prettier.io/blog/2017/11/07/1.8.0.html#markdown-support) to consistently format your Markdown code.

## Labels

- **Priorities**: In our experience, three labels suffice to order issues by their priorities.
  - ~prio:1: An issue whose resolution cannot be delayed (color `#FF0000`)
  - ~prio:2: An issue to be dealt with at the next opportunity, provided that there are no more open issues with ~"prio:1" (color `#D10069`)
  - ~prio:3: An issue to be dealt with at a later opportunity (color `#F2C1C1`)

## Issues

- **Title**: Formulate the issue's title in a positive way in imperative mood with a verb.<br>Example: _Reformulate all titles in a positive way in imperative mood with a verb._

## Merge Requests

- **Use Issue Close Patterns**: In case this merge request resolves an issue, state so in the merge request body via one of [GitLab's built-in close patterns](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).<br>Example: `Resolves #65.`

## Commits

- **Commit Message**: Formulate the commit message subject in imperative mood. Further best practices are given in [_The seven rules of a great Git commit message_](https://chris.beams.io/posts/git-commit/#seven-rules).
- **Use Subjects**: In case of a long commit message, summarise the changes in around 50 characters or less and separate the commit message's body by a blank line.
- **Use Issue Close Patterns**: In case this commit resolves an issue, state so via one of [GitLab's built-in close patterns](https://docs.gitlab.com/ee/user/project/issues/managing_issues.html#closing-issues-automatically).<br>Example: `Add new header (resolves #65)`

## Branches

- **Name**: If the branch is related to the issue with number `N`, its name should be prefixed by `N-`.<br>Example: _If you have an issue `#65`, the branch with name `65-some-feature` will be automatically linked to it in GitLab._
- **Default Name**: Prefer `main` over `master` for your default branch, following Git's defaults. A brief summary of the recent developments regarding the shift towards `main` instead of `master` is given in these articles of [GitLab](https://about.gitlab.com/blog/2021/03/10/new-git-default-branch-name/) and [GitHub](https://github.com/github/renaming).

## Files

- **Gitignore**: Use the templates from https://gitignore.io/ to create a reasonable `.gitignore` file.<br>Example: Use `gi node,python >> .gitignore` for a project with Node.js and Python.

## Miscellaneous

- **Your E-Mail Addresses**: GitLab links commits to the corresponding user based on the e-mail address (`git config user.email`). Thus make sure to add all your alternative e-mail addresses in your [profile settings](/-/profile/emails).
- **Public SSH Keys**: To access the public SSH keys of a user, navigate to their profile page, and add `.keys` to the URL. This can be useful when you want to enable users to login to a machine password-free via public key authentication.<br>Example: _[`https://gitlab.com/falco.nogatz.keys`](https://gitlab.com/falco.nogatz.keys) returns the public keys of the user `falco.nogatz`._
- **Familiarise Yourself with Git**: One of the best ways to accomodate people who are new to Git is the open source card game [_Oh my Git!_](https://ohmygit.org/).
- **Don't Repeat Yourself in GitLab CI/CD Pipelines**: YAML provides various ways to re-use snippets. Have a look at [this GitLab.com blogpost](https://about.gitlab.com/blog/2023/01/03/keeping-your-development-dry/#Minimizing%20duplication%20in%20CI/CD) to minimise duplication with the help of `include`, `extends`, `!reference`, and YAML anchors.
